/**
 * k4 Clientform plugin for Craft CMS
 *
 * K4Clientform_K4ClientformFieldType JS
 *
 * @author    Thomas Bauer, kreisvier communications ag
 * @copyright Copyright (c) 2016 Thomas Bauer, kreisvier communications ag
 * @link      http://www.kreisvier.ch
 * @package   K4Clientform
 * @since     0.9
 */

 ;(function ( $, window, document, undefined ) {

    var pluginName = "K4Clientform_K4ClientformFieldType",
        defaults = {
        };

    // Plugin constructor
    function Plugin( element, options ) {
        this.element = element;

        this.options = $.extend( {}, defaults, options) ;

        this._defaults = defaults;
        this._name = pluginName;

        this.init();
    }

    Plugin.prototype = {

        init: function(id) {
            var _this = this;

            $(function () {

/* -- _this.options gives us access to the $jsonVars that our FieldType passed down to us */

            });
        }
    };

    // A really lightweight plugin wrapper around the constructor,
    // preventing against multiple instantiations
    $.fn[pluginName] = function ( options ) {
        return this.each(function () {
            if (!$.data(this, "plugin_" + pluginName)) {
                $.data(this, "plugin_" + pluginName,
                new Plugin( this, options ));
            }
        });
    };

})( jQuery, window, document );
