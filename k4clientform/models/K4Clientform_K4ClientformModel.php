<?php
/**
 * k4 Clientform plugin for Craft CMS
 *
 * K4Clientform_K4Clientform Model
 *
 * --snip--
 * Models are containers for data. Just about every time information is passed between services, controllers, and
 * templates in Craft, it’s passed via a model.
 *
 * https://craftcms.com/docs/plugins/models
 * --snip--
 *
 * @author    Thomas Bauer, kreisvier communications ag
 * @copyright Copyright (c) 2016 Thomas Bauer, kreisvier communications ag
 * @link      http://www.kreisvier.ch
 * @package   K4Clientform
 * @since     0.9
 */

namespace Craft;

class K4Clientform_K4ClientformModel extends BaseModel
{
    /**
     * Defines this model's attributes.
     *
     * @return array
     */
    protected function defineAttributes()
    {
        return array_merge(parent::defineAttributes(), array(
            'k4CfrmField'     => array(AttributeType::String, 'default' => ''),
            'k4CfrmIdField'   => array(AttributeType::String, 'default' => md5(time())),
        ));
    }
}