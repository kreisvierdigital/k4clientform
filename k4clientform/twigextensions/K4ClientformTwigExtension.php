<?php
/**
 * k4 Clientform plugin for Craft CMS
 *
 * k4 Clientform Twig Extension
 *
 * --snip--
 * Twig can be extended in many ways; you can add extra tags, filters, tests, operators, global variables, and
 * functions. You can even extend the parser itself with node visitors.
 *
 * http://twig.sensiolabs.org/doc/advanced.html
 * --snip--
 *
 * @author    Thomas Bauer, kreisvier communications ag
 * @copyright Copyright (c) 2016 Thomas Bauer, kreisvier communications ag
 * @link      http://www.kreisvier.ch
 * @package   K4Clientform
 * @since     0.9
 */

namespace Craft;

use Twig_Extension;
use Twig_Filter_Method;

class K4ClientformTwigExtension extends \Twig_Extension
{
    /**
     * Returns the name of the extension.
     *
     * @return string The extension name
     */
    public function getName()
    {
        return 'K4Clientform';
    }

    /**
     * Returns an array of Twig filters, used in Twig templates via:
     *
     *      {{ 'something' | someFilter }}
     *
     * @return array
     */
    public function getFilters()
    {
        return array(
            'k4ClientFormHandler' => new \Twig_Filter_Method($this, 'k4SendClientFormHandler'),
        );
    }

    /**
     * Returns an array of Twig functions, used in Twig templates via:
     *
     *      {% set this = someFunction('something') %}
     *
     * @return array
     */
    public function getFunctions()
    {
        return array(
            'k4ClientFormHandler' => new \Twig_Function_Method($this, 'k4SendClientFormHandler'),
        );
    }

    /**
     * Our function called via Twig; it can do anything you want
     *
      * @return string
     */
    public function k4SendClientFormHandler($formContent = null,$eMailSender = "",$eMailRecipient = "",$eMailSubject = "", $eMailErrorMsg = "", $eMailSuccessMsg = "")
    {
        $content = "";
        if ($formContent){
            $content = $formContent["k4CfrmField"];
            $formIdKey = $formContent["k4CfrmIdField"];
        }
        // Prüfen zusätzliche Blindcopy?
        // Reine Textversion bei E-Mail sichergestellt?
        // Prüfen CSRF Token Handling...
        // Hinzufügen von Feld für Dateien
        
        
        if ($content != ""){
         
        $formHtml = "";
        $emailData = "";
        $formId = md5($content);
        $sent = false;
        $noError = true;
        $pluginSettings = craft()->plugins->getPlugin('K4Clientform')->getSettings();
        $honeyPotField = $pluginSettings->eMailHoneyPot;
           
            
            
        if (filter_var($eMailSender, FILTER_VALIDATE_EMAIL) === false){
            $eMailSender = $pluginSettings->eMailSender;
        }
        if (filter_var($eMailRecipient, FILTER_VALIDATE_EMAIL) === false){
            $eMailRecipient = $pluginSettings->eMailRecipient;
        }

            
        if ($eMailSubject == ""){
            $eMailSubject = $pluginSettings->eMailSubject;
        }
        
        
        if ($eMailErrorMsg == ""){
            $eMailErrorMsg = $pluginSettings->eMailErrorMsg;
        }
        
        if ($eMailSuccessMsg == ""){
            $eMailSuccessMsg = $pluginSettings->eMailSuccessMsg;            
        }


        
        
        if(isset($_POST[$formId]))
        {
           $sent = true;
           
            if (!$honeyPotField == ""){
                $requestHoneyPotField = $_POST[$honeyPotField];
                if (!$requestHoneyPotField == ""){
                    throw new HttpException(400);
                }

            }
            
            $requestToken = $_POST['CRAFT_CSRF_TOKEN'];
            if ($requestToken !== craft()->request->getCsrfToken()) {
                // Token is not valid
                
                throw new HttpException(400);
            }          
            
            
        }
            
    
        $formHtml = $formHtml. "<form method='post'><input type='hidden' name='". $formId."' value='yes'>";
        
        
        $formHtml = $formHtml . "<input type='hidden' name='CRAFT_CSRF_TOKEN' value='" . craft()->request->getCsrfToken(). "'>";
        $formHtml = $formHtml . "<div style='display:none'><input type='text' name='". $honeyPotField ."' value=''></div>";
        
        $html = str_get_html($content);
       
        $selectedFilter = 'field';
        //set selected link 
        $selectElemArr = $html->find($selectedFilter);
        

        $jsonElemStr = '';

        if (is_array($selectElemArr)){
           // Element wurde gefunden - Active Klassen setzen
                
            $lastType = '';

            foreach($selectElemArr as $element){
                
                $fieldData = "";
                
                if(isset($_POST[$element->name]) && $sent)
                {
                    $fieldData = str_replace("\"","&quot;",str_replace("'","&apos;",htmlentities($_POST[$element->name])));
                }
                
                $isRequired = $element->required;
                $isSelected = "";
                $isRequiredAttr = "";
                
                if ($isRequired){
                    $isRequiredAttr = "required";
                }
                $aditionalClass = "";                  

                switch ($element->type) {
       
                        // inputfeld
                        case "text":
                            if ($sent){
                                if ($isRequired && $fieldData == ""){
                                    $aditionalClass = $aditionalClass . " error ";
                                    $noError = false; 
                                }
                                $emailData = $emailData.  "<tr><td>" . html_entity_decode($element->label) . "</td><td>" . $fieldData . "</td></tr>"; 
                                $jsonElemStr .= ' {"' . $element->name . '":"' . $fieldData . '"},';
                             }
                        
                            $formHtml .= '
                                <div id="fields-' . $element->name . '-field" class="field plaintext ' . $element->name . '-field ' . $isRequiredAttr . '">
                                    <div class="heading">
                                        <label for="fields-' . $element->name . '">' . html_entity_decode($element->label) .  '</label>';
                                        if ($element->description == true) {
                                            $formHtml .= '<small class="k4Cfrm-description">' . html_entity_decode($element->description) . '</small>';
                                        }
                                        $formHtml .= '
                                    </div>
                                    <div class="input">
                                        <input type="text" id="fields-' . $element->name . '" class="' . $element->name . $aditionalClass . '" 
                                        value="' . $fieldData . '" name="' . $element->name . '" 
                                        data-validation="' . $isRequiredAttr . '">
                                    </div>
                                </div>';
                        break;                        
                        
                        
                        // textarea
                        case "textarea":
                            if ($sent){
                                if ($isRequired && $fieldData == ""){
                                    $aditionalClass = $aditionalClass . " error ";
                                    $noError = false; 
                                }
                                $emailData = $emailData.  "<tr><td>" . html_entity_decode($element->label) . "</td><td>" . $fieldData . "</td></tr>"; 
                                $jsonElemStr .= ' {"' . $element->name . '":"' . $fieldData . '"},';
                            }
                        
                            $formHtml .= '
                                <div id="fields-' . $element->name . '-field" class="field plaintext ' . $element->name . '-field ' . $isRequiredAttr . '">
                                    <div class="heading">
                                        <label for="fields-' . $element->name . '">' . html_entity_decode($element->label) .  '</label>';
                                        if ($element->description == true) {
                                            $formHtml .= '<small class="k4Cfrm-description">' . html_entity_decode($element->description) . '</small>';
                                        }
                                        $formHtml .= '
                                    </div>
                                    <div class="input">
                                        <textarea id="fields-' . $element->name . '" class="' . $element->name . $aditionalClass . '" 
                                        rows="4" 
                                        name="' . $element->name . '" 
                                        data-validation="' . $isRequiredAttr . '">' . $fieldData . '</textarea>
                                    </div>
                                </div>';
                        break;
                        
                        
                        // checkbox
                        case "checkbox":
                            if ($sent){
                                if ($fieldData == "true"){
                                    $isSelected = "checked='checked'";
                                    $emailData = $emailData.  "<tr><td>" . html_entity_decode($element->label) . "</td><td>isChecked</td></tr>";
                                    $jsonElemStr .= ' {"' . $element->name . '":"0"},';
                                } 
                                else{
                                    if ($isRequired){
                                        $aditionalClass = $aditionalClass . " error ";
                                        $noError = false;                                    
                                    }
                                    $emailData = $emailData.  "<tr><td>" . html_entity_decode($element->label) . "</td><td>notChecked</td></tr>";    
                                    $jsonElemStr .= ' {"' . $element->name . '":"1"},';
                                }
                            }
                            
                            $formHtml .= '
                                <div id="fields-' . $element->name . '-field" class="field checkboxes ';
                                if ($lastType == 'checkbox') {
                                    $formHtml .= ' checkboxgroup ';
                                }
                                 $formHtml .= $element->name . '-field  ' . $isRequiredAttr . '">
                                 <div class="heading"></div>
                                    <div class="input">
                                        <div id="fields-' . $element->name . '" class="' . $element->name . '">
                                        <label for="fields-' . $element->name . '-1"><input type="checkbox" ' . $isRequiredAttr . ' ' . $isSelected . ' id="fields-' . $element->name . '-1" name="' . $element->name . '" value="true">&nbsp;' . html_entity_decode($element->label) . '</label>
                                        ';
                                        if ($element->description == true) {
                                            $formHtml .= '<small class="k4Cfrm-description">' . html_entity_decode($element->description) . '</small>';
                                        }
                                        $formHtml .= '</div>
				                    </div>
				                </div>';           
                        break;
                        
                        
                        
                        // radiobutton
                        case "radio-group":
                            if ($sent){
                                $selectedOption = "";
                                //remove all selected values, set selected value
                                foreach($element->find('option') as $optionElement){
                                    $optionElement->selected = null;  
                                    if ($optionElement->value == $fieldData){
                                        $optionElement->selected = "true";
                                        $selectedOption = $optionElement->innertext;
                                    }
                                }   
                                if ($isRequired && $fieldData == ""){
                                    $aditionalClass = $aditionalClass . " error ";
                                    $noError = false;                                    
                                }
                                $emailData = $emailData.  "<tr><td>" . html_entity_decode($element->label) . "</td><td>". $selectedOption ."</td></tr>";
                                $jsonElemStr .= ' {"' . $element->name . '":"' . $selectedOption . '"},';
                            }
                        
                        
                            $formHtml .= ' <div id="fields-' . $element->name . '-field" class="field radiobuttons ' . $element->name . '-field ' . $isRequiredAttr . '">
                                        <div class="heading"><span>' . html_entity_decode($element->label) . '</span>';
                                        if ($element->description == true) {
                                            $formHtml .= '<small class="k4Cfrm-description">' . html_entity_decode($element->description) . '</small>';
                                        }
                            $formHtml .= ' 
                                        </div>
                                        <div class="input"><div id="fields-' . $element->name . '" class="' . $element->name . '">';
                            $counter = 0;
                            foreach($element->find('option') as $optionElement){
                                $counter++;
                                $isSelected = "";
                                if ($optionElement->selected === "true" OR $counter == 1){
                                    $isSelected = "checked";  
                                }                                                               
                                $formHtml .= ' 
                                <label for="fields-' . $element->name . '-' . $counter . '"><input type="radio" id="fields-' . $element->name . '-' . $counter . '" name="' . $element->name . '" value="' . $optionElement->value . '" ' . $isSelected . '>' . $optionElement->innertext . '</label>
                                ';
                            }
                            $formHtml = $formHtml . '</div></div></div>';
                        break;


                        // selectfeld
                        case "select":
                            if ($sent){
                                $selectedOption = "";
                                //remove all selected values, set selected value
                                foreach($element->find('option') as $optionElement){
                                    $optionElement->selected = null;  
                                    if ($optionElement->value == $fieldData){
                                        $optionElement->selected = "true";
                                        $selectedOption = $optionElement->innertext;
                                    }
                                }   
                                if ($isRequired && $fieldData == ""){
                                    $aditionalClass = $aditionalClass . " error ";
                                    $noError = false;                                    
                                } 
                                $emailData = $emailData.  "<tr><td>" . html_entity_decode($element->label) . "</td><td>". $selectedOption ."</td></tr>";
                                $jsonElemStr .= ' {"' . $element->name . '":"' . $selectedOption . '"},';
                            }

                            $formHtml = $formHtml . '<div id="fields-' . $element->name . '-field" class="field dropdown ' . $element->name . '-field ' . $isRequiredAttr . '">
				                        <div class="heading"><span for="fields-' . $element->name . '">' . html_entity_decode($element->label) . '</span>';
                                        if ($element->description == true) {
                                            $formHtml = $formHtml . '<small class="k4Cfrm-description">' . html_entity_decode($element->description) . '</small>';
                                        }
                            $formHtml = $formHtml . '</div>
                                        <div class="input">
                                            <select id="fields-' . $element->name . '" class="' . $element->name . '" name="' . $element->name . '" ' . $isRequired . '>';
                            foreach($element->find('option') as $optionElement){
                                 $isSelected = "";
                                 if ($optionElement->selected === "true"){
                                   $isSelected = "selected='true'";  
                                 }                                 
                                 $formHtml = $formHtml . "<option name='" . $element->name . "' value='". $optionElement->value ."' ". $isSelected .">". $optionElement->innertext ."</option>";
                            }
                            $formHtml = $formHtml . '</select></div></div>';
                        break;

                        // p-tag
                        case "paragraph":
                            $formHtml = $formHtml . "<p>". html_entity_decode($element->label) . "</p>";
                            $emailData = $emailData. "<tr><td colspan='2'><p>".  html_entity_decode($element->label) ."</p></td></tr>";
                        break;
                        
                        // h2-tag
                        case "header":
                            $formHtml = $formHtml . "<h2>". html_entity_decode($element->label) . "</h2>";
                                if ($sent){
                                   $emailData = $emailData. "<tr><td colspan='2'><h2>".  html_entity_decode($element->label) ."</h2></td></tr>";
                                }
                        break;
                        
                    }

                    $lastType = $element->type;
               
            }
            
            $emailData = '<table>' . $emailData . '</table>';
    
            $oldPath = craft()->path->getTemplatesPath();
            craft()->path->setTemplatesPath($oldPath);
            if (craft()->templates->doesTemplateExist('emailtemplate')) {
                // template existiert unter craft/templates           
                $eMailTemplate = craft()->templates->render('emailtemplate', [ 'form' => $emailData, 'subject' => $eMailSubject ]);
            } else {
                // template existiert nur im plugin               
                $newPath = craft()->path->getPluginsPath().'k4clientform/templates';
                craft()->path->setTemplatesPath($newPath);
                $eMailTemplate = craft()->templates->render('emailtemplate', [ 'form' => $emailData, 'subject' => $eMailSubject ]);
            }
            craft()->path->setTemplatesPath($oldPath);
            
            $eMailTemplate = str_replace("%", "&#37;", $eMailTemplate);
            $eMailTemplate = str_replace("{", "&#123;", $eMailTemplate);
            $eMailTemplate = str_replace("}", "&#125;", $eMailTemplate);
                
            
            $formHtml =  $formHtml . '<div class="submit"><input type="submit" value="Submit"></div></form>';
            
        
            if ($sent && $noError){
                
                $jsonElemStr = rtrim($jsonElemStr, ",");

                // daten in db speichern
                $json = '{"form": {
                  "formId": "' . $formIdKey . '",
                    "data": [
                      ' . $jsonElemStr . '
                    ]
                }}';

                craft()->db->createCommand()->insert('k4_clientform_data', array(
                    'formId'    => $formIdKey,
                    'url'       => $_SERVER['PHP_SELF'],
                    'json'      => $json
                ));


                $email = new EmailModel();
                $eMailBcc = $pluginSettings->eMailBcc;
        
                $email->toEmail     = $eMailRecipient;
                $email->bcc         = array(array('email' => $eMailBcc, 'name' => $eMailBcc));
                $email->fromEmail   = $eMailSender;
                $email->subject     = $eMailSubject;
                $email->body        = $eMailTemplate;
                
                craft()->email->sendEmail($email);
                
                $formHtml = $eMailSuccessMsg;
                
                
                //                $formHtml =  $formHtml . "<p>E-Mail versenden, Dankesmeldung</p>";
//                $formHtml =  $formHtml . "<h2>E-Mail Inhalt</h2>" . $emailData ;   
//                
//                            
//               
                // $formHtml =  $formHtml ."<br>". $eMailSender;
                // $formHtml =  $formHtml ."<br>". $eMailRecipient;
                // $formHtml =  $formHtml ."<br>". $eMailSubject;
                // $formHtml =  $formHtml ."<br>". $eMailTemplate; 
                // $formHtml =  $formHtml ."<br>". $eMailErrorMsg;
                // $formHtml =  $formHtml ."<br>". $eMailSuccessMsg;
                
            }
            if ($sent && !$noError){
                $formHtml =  $eMailErrorMsg . $formHtml;                  
            }
                
        } else
        {
            $formHtml = $content . "<!-- Error, no valid form -->";
        }

            
        
        return $formHtml;
    }
            
        }
        
       
}