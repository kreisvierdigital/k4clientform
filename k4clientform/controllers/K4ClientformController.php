<?php
namespace Craft;

class K4ClientformController extends BaseController {

    public function actionDownload()
    {
        
        // the user needs permission to view the asset
        $permission = 'k4clientformSettings';
        // check if the current user has the required permissions
        $hasAccess = craft()->userSession->checkPermission($permission);

        if ($hasAccess){

            
           $rows = craft()->db->createCommand()
                ->select('handle')
                ->from('fields')
                ->where('type = :type', array(':type' => 'K4Clientform_K4Clientform'))
                ->queryAll();
            if($rows)
            {
                foreach($rows as $row)
                {
                    $column = $row['handle'];
                }
            }
            
            $column = 'field_' . $column;
            
            $formIdKey = $this->actionParams['variables']['matches']['id'];
            $format = $this->actionParams['variables']['matches']['directory'];

            $rows = craft()->db->createCommand()
                ->select($column)
                ->from('content')
                ->where(array('like', $column, '%'.$formIdKey.'%'))
                ->queryAll();
            if($rows)
            {
                foreach($rows as $row)
                {
                
                    $field_myform = $row[$column];
                
                }
            }
            
            //$html = '<img border="0" src="/images/image.jpg" alt="Image" width="100" height="100" />';

            $field_myform = str_replace('\r', '', $field_myform);
            $field_myform = str_replace('\n', '', $field_myform);
            $field_myform = str_replace('\t', '', $field_myform);
            $field_myform = str_replace('\\', '', $field_myform);
            $field_myform = str_replace('{"k4CfrmField":"<form-template><fields>', '', $field_myform);
            
            $arrFormFields = array();
            
            $field_myform_array = explode("</field>", $field_myform);
            for($i=0;$i<sizeof($field_myform_array)-1;$i++) {
                preg_match( '@label="([^"]+)"@' , $field_myform_array[$i], $match );
                $label = array_pop($match);
                preg_match( '@name="([^"]+)"@' , $field_myform_array[$i], $match );
                $name = array_pop($match);
                $arrFormFields[$name] = $label;
            }
            
            $rows = craft()->db->createCommand()
                ->select('*')
                ->from('k4_clientform_data')
                ->where('formId=:formId', array(':formId'=>$formIdKey))
                ->queryAll();

        
            $csvFileinhalt = '';
            $xlsFileinhalt = '';
            $filename = 'K4Clientform_' . date('Ymd');
            if ($format == 'csv') {
                header("Content-Type: text/csv; charset=utf-8");
                header("Content-Disposition: attachment; filename=" . $filename . ".csv");
                header("Expires: 0");
                header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
                header("Cache-Control: private",false);                
            } else if ($format == 'xls') {
                header("Content-Type: application/vnd.ms-excel; charset=utf-8");
                header("Content-Disposition: attachment; filename=" . $filename . ".xls");
                header("Expires: 0");
                header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
                header("Cache-Control: private",false);                
            } else if ($format == 'html') {
                header("Content-Type: text/html; charset=utf-8");
                header("Content-Disposition: attachment; filename=" . $filename . ".html");
                header("Expires: 0");
                header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
                header("Cache-Control: private",false);                
            }

            $xlsFileinhalt .= '<table>';
            $counter = 0;
            $lastHeaderStringXLS = '';
            $lastHeaderStringCSV = '';
            if($rows)
            {
                foreach($rows as $row)
                {
                
                    $jsonDB = JsonHelper::decode($row['json']);
                    $data = $jsonDB['form']['data'];

                    // keys ausgeben
                    $headerStringXLS = '';
                    $headerStringCSV = '';
                    //if ($counter == 0) {
    
                        $headerStringXLS .= '<tr>';
                        $headerStringXLS .= '<td style="border:1px solid black;">URL</td>';  
                        $headerStringXLS .= '<td style="border:1px solid black;">Date / Time</td>';  
                        $headerStringCSV .= 'URL;Date / Time;';  

                        for ($i=0;$i<sizeof($data);$i++) {
                            $innerData = $data[$i];
                            $idHead = implode(" ", array_keys($innerData));
                            $headerStringXLS .= '<td style="border:1px solid black;">';   
                            $headerStringXLS .= $arrFormFields[$idHead];
                            $headerStringXLS .= '</td>';
                            $headerStringCSV .= $arrFormFields[$idHead] . ';';
                        }

                        $headerStringXLS .= '</tr>';
                        $headerStringCSV .= "\r\n";
                        
                    //}
    
                    if ($lastHeaderStringXLS != $headerStringXLS) {
                        $xlsFileinhalt .= $headerStringXLS;
                        $csvFileinhalt .= $headerStringCSV;
                    }
                    
                    $xlsFileinhalt .= '<tr>';
                        $xlsFileinhalt .= '<td style="border:1px solid black;">' . $row['url'] . '</td>';  
                        $xlsFileinhalt .= '<td style="border:1px solid black;">' . $row['dateCreated'] . '</td>';  
                    $csvFileinhalt .= $row['url'] . ';' . $row['dateCreated'] . ';';  

                    for ($i=0;$i<sizeof($data);$i++) {
                        $innerData = $data[$i];
                        $xlsFileinhalt .= '<td style="border:1px solid black;">';   
                        $xlsFileinhalt .= implode(" ", ($innerData));
                        $xlsFileinhalt .= '</td>';
                        $csvFileinhalt .= implode(";", ($innerData)) . ';';
                    }

                    $xlsFileinhalt .= '</tr>';                    
                    $csvFileinhalt .= "\r\n";

                    $counter++;
                    
                    $lastHeaderStringXLS = $headerStringXLS;
                    $lastHeaderStringCSV = $headerStringCSV;

                }
            }
            $xlsFileinhalt .= '</table>';
            
            if ($format == 'csv') {
                echo $csvFileinhalt;            
            } else if ($format == 'xls') {
                echo $xlsFileinhalt;            
            } else if ($format == 'html') {
                echo $xlsFileinhalt;            
            }                
        
            exit;
            
        } else{
            // throw a 404 exception
            throw new HttpException(404, "File not found or permission denied");  
        }
        
    }


}