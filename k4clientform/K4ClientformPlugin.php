<?php
/**
 * k4 Clientform plugin for Craft CMS
 *
 * k4 Clientform Description
 *
 * --snip--
 * Craft plugins are very much like little applications in and of themselves. Weâ€™ve made it as simple as we can,
 * but the training wheels are off. A little prior knowledge is going to be required to write a plugin.
 *
 * For the purposes of the plugin docs, weâ€™re going to assume that you know PHP and SQL, as well as some semi-
 * advanced concepts like object-oriented programming and PHP namespaces.
 *
 * https://craftcms.com/docs/plugins/introduction
 * --snip--
 *
 * @author    Thomas Bauer, kreisvier communications ag
 * @copyright Copyright (c) 2016 Thomas Bauer, kreisvier communications ag
 * @link      http://www.kreisvier.ch
 * @package   K4Clientform
 * @since     0.9
 */

namespace Craft;

class K4ClientformPlugin extends BasePlugin
{
    /**
     * Called after the plugin class is instantiated; do any one-time initialization here such as hooks and events:
     *
     * craft()->on('entries.saveEntry', function(Event $event) {
     *    // ...
     * });
     *
     * or loading any third party Composer packages via:
     *
     * require_once __DIR__ . '/vendor/autoload.php';
     *
     * @return mixed
     */
    public function init()
    {
       require_once "library/simple_html_dom.php";
    }

    /**
     * Returns the user-facing name.
     *
     * @return mixed
     */
    public function getName()
    {
         return Craft::t('k4 Clientform');
    }

    /**
     * Plugins can have links to their documentation on the Plugins page by adding a getDocumentationUrl() method on
     * the primary plugin class:
     *
     * @return string
     */
    public function getDocumentationUrl()
    {
        return 'https://bitbucket.org/kreisvierdigital/k4clientform';
    }

    /**
     * Returns the version number.
     *
     * @return string
     */
    public function getVersion()
    {
        return '0.9.3';
    }

    /**
     * As of Craft 2.5, Craft no longer takes the whole site down every time a pluginâ€™s version number changes, in
     * case there are any new migrations that need to be run. Instead plugins must explicitly tell Craft that they
     * have new migrations by returning a new (higher) schema version number with a getSchemaVersion() method on
     * their primary plugin class:
     *
     * @return string
     */
    public function getSchemaVersion()
    {
        return '0.9';
    }

    /**
     * Returns the developerâ€™s name.
     *
     * @return string
     */
    public function getDeveloper()
    {
        return 'Thomas Bauer, kreisvier communications ag';
    }

    /**
     * Returns the developerâ€™s website URL.
     *
     * @return string
     */
    public function getDeveloperUrl()
    {
        return 'http://www.kreisvier.ch';
    }

    /**
     * Returns whether the plugin should get its own tab in the CP header.
     *
     * @return bool
     */
    public function hasCpSection()
    {
        return false;
    }

    /**
     * Add any Twig extensions.
     *
     * @return mixed
     */
    public function addTwigExtension()
    {
        Craft::import('plugins.k4clientform.twigextensions.K4ClientformTwigExtension');

        return new K4ClientformTwigExtension();
    }

    /**
     * Called right before your pluginâ€™s row gets stored in the plugins database table, and tables have been created
     * for it based on its records.
     */
    public function onBeforeInstall()
    {
        
        $table = 'k4_clientform_data';

        if (!craft()->db->tableExists($table))
        {

            craft()->db->createCommand()->createTable($table, array(
                'formId' => array('column' => ColumnType::Text),
                'url'    => array('column' => ColumnType::Text),
                'json'     => array('column' => ColumnType::Text)
            ));

        }

    }

    /**
     * Called right after your pluginâ€™s row has been stored in the plugins database table, and tables have been
     * created for it based on its records.
     */
    public function onAfterInstall()
    {
        craft()->request->redirect(UrlHelper::getCpUrl('settings/plugins/k4clientform'));
    }

    /**
     * Called right before your pluginâ€™s record-based tables have been deleted, and its row in the plugins table
     * has been deleted.
     */
    public function onBeforeUninstall()
    {
        $table = 'k4_clientform_data';
        if (craft()->db->tableExists($table))
        {
            craft()->db->createCommand()->dropTable($table);
        }  
    }

    /**
     * Called right after your pluginâ€™s record-based tables have been deleted, and its row in the plugins table
     * has been deleted.
     */
    public function onAfterUninstall()
    {
    }

    /**
     * Defines the attributes that model your pluginâ€™s available settings.
     *
     * @return array
     */
    protected function defineSettings()
    {
        return array(
            'eMailSender'       => array(AttributeType::String, 'label' => 'E-Mail sender address',     'default' => craft()->systemSettings->getSetting('email', 'emailAddress')),
            'eMailRecipient'    => array(AttributeType::String, 'label' => 'E-Mail recipient address',  'default' => craft()->systemSettings->getSetting('email', 'emailAddress')),
            'eMailBcc'          => array(AttributeType::String, 'label' => 'E-Mail BCC recipient',      'default' => ''),
            'eMailSubject'      => array(AttributeType::String, 'label' => 'E-Mail subject',            'default' => 'k4 Clientform Email'),
            'eMailErrorMsg'     => array(AttributeType::String, 'label' => 'E-Mail error message',      'default' => 'k4 Clientform error message'),
            'eMailSuccessMsg'   => array(AttributeType::String, 'label' => 'E-Mail success message',    'default' => 'k4 Clientform success message'),
            'eMailHoneyPot'     => array(AttributeType::String, 'label' => 'E-Mail honeypot',           'default' => 'k4-secure'),
        );
    }

    /**
     * Returns the HTML that displays your pluginâ€™s settings.
     *
     * @return mixed
     */
    public function getSettingsHtml()
    {
       return craft()->templates->render('k4clientform/K4Clientform_Settings', array(
           'settings' => $this->getSettings()
       ));
    }
    
    /**
     * If you need to do any processing on your settingsâ€™ post data before theyâ€™re saved to the database, you can
     * do it with the prepSettings() method:
     *
     * @param mixed $settings  The Widget's settings
     *
     * @return mixed
     */
    public function prepSettings($settings)
    {
        // Modify $settings here...

        return $settings;
    }
    

	/**
	 * @return array
	 */
	public function registerUserPermissions()
	{
		return array(
			'k4clientformSettings' => array(
				'label' => Craft::t('Export K4Clientform Data')
			)
		);
	}  
    
    
    /**
    * Register control panel routes
    */
    public function registerSiteRoutes()
    {
        // NOTE: the second route is used to capture files in subdirectories as well
        return array(
            'k4clientform/(?P<directory>.*)/(?P<id>[a-f0-9]{32})' => array('action' => 'k4Clientform/download'),
        );
    }
    
    public function getReleaseFeedUrl()
    {
        return 'https://bitbucket.org/kreisvierdigital/k4clientform/raw/master/k4clientform/releases.json';
    }    
    
}