<?php
/**
 * k4 Clientform plugin for Craft CMS
 *
 * k4 Clientform Translation
 *
 * @author    Thomas Bauer, kreisvier communications ag
 * @copyright Copyright (c) 2016 Thomas Bauer, kreisvier communications ag
 * @link      http://www.kreisvier.ch
 * @package   K4Clientform
 * @since     0.9
 */

return array(
    
       'k4Cfrm-addOption' => 'Option hinzufügen',
       'k4Cfrm-cannotBeEmpty' => 'Dieses Feld kann nicht leer sein.',
       'k4Cfrm-checkbox' => 'Checkbox',
       'k4Cfrm-checkboxes' => 'Checkboxes',
       'k4Cfrm-clearAll' => 'Löschen',
       'k4Cfrm-close' => 'Schliessen',
       'k4Cfrm-content' => 'Inhalt',
       'k4Cfrm-description' => 'Hilfetext',
       'k4Cfrm-descriptionField' => 'Beschreibung',
       'k4Cfrm-editorTitle' => 'Formular Elemente',
       'k4Cfrm-fieldNonEditable' => 'Dieses Feld kann nicht bearbeitet werden.',
       'k4Cfrm-formUpdated' => 'Formular aktualisiert',
       'k4Cfrm-getStarted' => 'Ziehen Sie die gewünschten Formularfelder in diesen Bereich',
       'k4Cfrm-header' => 'Überschrift',
       'k4Cfrm-hide' => 'Bearbeiten',
       'k4Cfrm-hidden' => 'Versteckter Input',
       'k4Cfrm-label' => 'Name',
       'k4Cfrm-labelEmpty' => 'Die Feldbezeichnung kann nicht leer sein.',
       'k4Cfrm-mandatory' => 'Zwingend',
       'k4Cfrm-maxlength' => 'Maximale Länge',
       'k4Cfrm-minOptionMessage' => 'Dieses Feld benötigt mindestens zwei Optionen.',
       'k4Cfrm-name' => 'Name',
       'k4Cfrm-no' => 'Nein',
       'k4Cfrm-off' => 'Aus',
       'k4Cfrm-on' => 'An',
       'k4Cfrm-option' => 'Option',
       'k4Cfrm-optional' => 'optional',
       'k4Cfrm-optionLabelPlaceholder' => 'Name',
       'k4Cfrm-optionValuePlaceholder' => 'Wert',
       'k4Cfrm-optionEmpty' => 'Option ist zwingend',
       'k4Cfrm-paragraph' => 'Absatz',
       'k4Cfrm-placeholder' => 'Platzhalter',
       'k4Cfrm-placeholders' => "{
           value : 'Wert',
           label : 'Bezeichnung',
           text : '',
           textarea : '',
           email : 'Bitte E-Mail eingeben',
           placeholder : '',
           className : 'leertaste um mehrere Klassen zu verwenden',
           password : 'Passwort eingeben'
       }",
       'k4Cfrm-preview' => 'Vorschau',
       'k4Cfrm-radioGroup' => 'Optionsfelder',
       'k4Cfrm-removeMessage' => 'Entferne Element',
       'k4Cfrm-remove' => '&#215;',
       'k4Cfrm-required' => 'Zwingend',
       'k4Cfrm-roles' => 'Zugriff',
       'k4Cfrm-save' => 'Speichern',
       'k4Cfrm-selectOptions' => 'Optionen',
       'k4Cfrm-select' => 'Auswahl',
       'k4Cfrm-selectionsMessage' => 'Mehrfache Auswahl zulassen',
       
       'k4Cfrm-subtype' => 'Type',

       'k4Cfrm-text' => 'Textfeld',
       'k4Cfrm-textArea' => 'Mehrzeiliges Textfeld',
       'k4Cfrm-toggle' => 'Ausgewählt',
       'k4Cfrm-warning' => 'Achtung!',
       'k4Cfrm-yes' => 'Ja',
    
    
       'k4Cfrm-submitButton' => 'Absenden'
);
