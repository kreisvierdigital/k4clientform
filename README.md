# k4 Clientform plugin for Craft CMS

Plugin that allows you to create forms by drag&drop in your CMS content. (Forms can be sent by users and the form-data can be exported in the backend.)

![k4_form_ani.gif](https://bitbucket.org/repo/jMAXAE/images/3317331375-k4_form_ani.gif)

## Installation k4 Clientform

To install k4 Clientform plugin, follow these steps (or use our installation video: https://vimeo.com/172583388/e419dc3974):

1. Download & unzip k4 Clientform plugin and move the `k4clientform` directory into your `craft/plugins` directory
2. Install plugin in the Craft Control Panel under Settings > Plugins
3. Adjust the clientform settings and enter default email-adresses for sender & recipient, the email subject and the messages that will be shown to the user after sending the form.
4. Move the CSS-folder (and if you want to use the js-form-validator also the JS-folder) from the `_k4clientform_css_js_you_have_to_move_me` to your proper CSS/JS-directory.

## Using k4 Clientform

To enable the plugin on a certain section, follow these steps:

1. go to: 	Settings > Fields > New field:
2. add: 	Name: `myTestFormName` / Handle: `myTestFormHandle` / Field Type: `k4Clientform`
3. go to: 	Settings > Sections > Edit entry type:
4. add: 	`myTestFormHandle` to your content
5. open your template in your favourite editor & add the following code:
```
<div class="k4Cfrm-wrapper k4Cfrm-wrapper-top2bottom">{{ k4ClientFormHandler(entry.myTestFormHandle) | raw }}</div>
{% includeCssFile "/pathToMyCss/k4clientform/frontend-default.css" %}
```

* to change the layout to left to right, change the following css-class:
```
<div class="k4Cfrm-wrapper k4Cfrm-wrapper-left2right">{{ k4ClientFormHandler(entry.myTestFormHandle) | raw }}</div>
{% includeCssFile "/pathToMyCss/k4clientform/frontend-default.css" %}
```

* to enable js-form-validation include jquery.form-validator, add the following code:
```
{% includeJsFile "/pathToMyJs/k4clientform/form-validator/jquery.form-validator.js"%}
{% set formJs%}
    $("input[required]").attr("data-validation","required").removeAttr("required");
    $("textarea[required]").attr("data-validation","required").removeAttr("required");
    $.validate({
        lang : '{{ craft.locale }}'
    });
{% endset %}
{% includeJs formJs %}
```

* alternative: include the css/js the old-fashioned way in your html-head.

## Permissions for k4 Clientform (optional)

To give users the rights to export the form-data:

1. go to Settings > Users > User Groups
2. activate `Export K4Clientform Data` for `k4 Clientform`

## Change email-template for k4 Clientform (optional)

To change the template for the emails that are sent after filling out the form:

1. go to `/k4clientform/templates/emailtemplate.html`
2. change the template
3. make sure you didn't delete `{{subject | raw}}` and `{{form | raw}}` from the template


## k4 Clientform Changelog

### 0.92 -- 28-Jun-2016 (friedrich@kreisvier.ch)

* making k4clientform ready to be released
* adding emailTemplate 
* adding possibility to download data from forms via backend for analysis
* fixing bugs

### 0.9 -- 2016.05.06

* Initial release

Brought to you by [Thomas Bauer, kreisvier communications ag](http://www.kreisvier.ch)


## Credits
* jQuery formBuilder
* PHP Simple HTML DOM Parser
* jQuery Form Validator